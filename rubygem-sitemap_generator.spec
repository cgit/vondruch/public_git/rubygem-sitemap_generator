# Generated from sitemap_generator-5.3.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name sitemap_generator

Name: rubygem-%{gem_name}
Version: 6.0.0
Release: 1%{?dist}
Summary: Easily generate XML Sitemaps
License: MIT
URL: http://github.com/kjvarga/sitemap_generator
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/kjvarga/sitemap_generator.git && cd sitemap_generator
# git checkout v6.0.0 && tar czvf sitemap_generator-6.0.0-specs.tar.gz spec/
Source1: %{gem_name}-%{version}-specs.tar.gz
Source2: https://raw.githubusercontent.com/kjvarga/sitemap_generator/v6.0.0/config/sitemap.rb
Requires: rubygem(bigdecimal)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(bigdecimal)
BuildRequires: rubygem(builder)
BuildRequires: rubygem(fog-aws)
BuildRequires: rubygem(nokogiri)
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(webmock)
BuildArch: noarch

%description
SitemapGenerator is a framework-agnostic XML Sitemap generator written in Ruby
with automatic Rails integration.  It supports Video, News, Image, Mobile,
PageMap and Alternate Links sitemap extensions and includes Rake tasks for
managing your sitemaps, as well as many other great features.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n  %{gem_name}-%{version} -b 1

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/



%check
pushd .%{gem_instdir}
ln -s %{_builddir}/spec .

mkdir config
ln -s %{SOURCE2} ./config

# Get rid of Bundler.
sed -i '/[bB]undler/ s/^/#/' spec/spec_helper.rb

# We don't need byebug to run the test suite.
sed -i "/require 'byebug'/ s/^/#/" spec/spec_helper.rb

# The 'fog-aws' is awailable in fog-aws 1.4.0+, but we have just 1.2.1 ATM :/
sed -i "/require 'fog-aws'/ s|fog-aws|fog/aws|" spec/sitemap_generator/adapters/s3_adapter_spec.rb

# We don't have aws-sdk-s3 in Fedora yet.
mv spec/sitemap_generator/adapters/aws_sdk_adapter_spec.rb{,.disable}

rspec -rsitemap_generator spec
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_instdir}/VERSION
%{gem_libdir}
%{gem_instdir}/rails
%{gem_instdir}/templates
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGES.md
%doc %{gem_instdir}/README.md

%changelog
* Tue Nov 28 2017 Vít Ondruch <vondruch@redhat.com> - 6.0.0-1
- Update to sitemap_generator 6.0.0.

* Mon Sep 11 2017 Vít Ondruch <vondruch@redhat.com> - 5.3.1-1
- Initial package
